﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof (Rigidbody2D))]
public class PlayerJump : MonoBehaviour {


	public float jumpSpeed = 5f;
	public float jumpLimit = 5f;
	public Text gameOver;
	private Rigidbody2D _rigidbody2D;


	private bool onGround;
	public Rect groundRect = new Rect(-0.32f, -0.72f, 0.56f, 0.1f);
	public LayerMask groundLayerMask = -1;



	void Start() {
		_rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
		gameOver.enabled = false;
	}

	// Update is called once per frame
	void Update () {

		Debug.Log ("Timescale = " + Time.timeScale);
		CheckOnGround();

		Vector2 v = _rigidbody2D.velocity;

		if (onGround && Input.GetButtonDown ("Jump")) {

			v.y = jumpSpeed;

		}

		if (Input.GetKeyDown (KeyCode.R)) {
			Application.LoadLevel (0);
			Time.timeScale = 1f;

		}
		if (Time.timeScale == 0) {
			gameOver.enabled = true;
			return;
		}
		_rigidbody2D.velocity = v;

	}

	private void CheckOnGround() {

		onGround = false;

		Vector2 min = new Vector2(transform.position.x, transform.position.y) + groundRect.min;
		Vector2 max = new Vector2(transform.position.x, transform.position.y) + groundRect.max;

		Collider2D collider = Physics2D.OverlapArea(min, max, groundLayerMask);

		onGround = collider != null;

	}



	void OnDrawGizmos() {
		Vector3 centre = transform.position;
		centre.x += groundRect.center.x;
		centre.y += groundRect.center.y;

		Vector3 size = Vector3.zero;
		size.x += groundRect.width;
		size.y += groundRect.height;

		if (onGround) {
			Gizmos.color = Color.red;
		}
		else {
			Gizmos.color = Color.white;
		}
		Gizmos.DrawWireCube(centre, size);

	}

}
