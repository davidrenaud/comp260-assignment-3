﻿using UnityEngine;
using System.Collections;

public class DestroyAll : MonoBehaviour {

	public LayerMask DestroyerLayer;

	private bool isGone = false;

	// Use this for initialization
	void Start () {

	}
	// Update is called once per frame
	void Update () {

	}
	public void OnTriggerEnter2D(Collider2D collider) {		

		if (isGone) return;

		if (((1 << collider.gameObject.layer) & DestroyerLayer.value) != 0) {
			Destroy(collider.GetComponent<Collider2D>().gameObject);
		}
	}
}