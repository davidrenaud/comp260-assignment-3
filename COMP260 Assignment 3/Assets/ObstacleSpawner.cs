﻿using UnityEngine;
using System.Collections;

public class ObstacleSpawner : MonoBehaviour {
	public GameObject obstaclePrefab;

	private bool ready = true;
	int randomNum = 200;
	int count = 0;
	public int score = 0;


	// Use this for initialization
	void Start () {
		
	}
	// Update is called once per frame
	void Update () {

		if (count >= randomNum) {
			GameObject obstacle = Instantiate (obstaclePrefab);
			randomNum = Random.Range (60,150);
			count = 0;

		} else
			count++;
	}

}

