﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TagPickup : MonoBehaviour {

	public LayerMask tagLayer;
	public LayerMask DestroyerLayer;

	private AudioSource _audio;
	private bool isGone = false;

	public Text scoreText;

	public float Score = 0;



	public void Start() {
		_audio = gameObject.GetComponent<AudioSource>();
	}

	public void OnTriggerEnter2D(Collider2D collider) {		
		if (isGone) return;

		// check if the colliding object is in the playerLayer
		if (((1 << collider.gameObject.layer) & tagLayer.value) != 0) {
			// play a sound if there is one
			if (_audio != null) {
				_audio.Play();
			}


			// hide the sprite and disable this script
			Destroy(collider.GetComponent<Collider2D>().gameObject);
		}
		Score += 1;
		Time.timeScale += 0.05f;
		scoreText.text = ("Score: " + Score);
		Debug.Log ("Score: " + Score);

	}

}
