﻿using UnityEngine;
using System.Collections;

public class TagSpawner : MonoBehaviour {
	public GameObject obstaclePrefab;

	private bool ready = true;
	int randomNum = 300;
	int count = 0;



	// Use this for initialization
	void Start () {
		
	}
	// Update is called once per frame
	void Update () {

		if (count >= randomNum) {
			GameObject obstacle = Instantiate (obstaclePrefab);
			randomNum = Random.Range (250,500);
			count = 0;

		} else
			count++;
	}

}

